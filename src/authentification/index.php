<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
<head>
    <title>CronChart test</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <style type="text/css">
    * { font-family:verdana,arial,sans-serif; font-size:14px; }
    .cron { border:1px dotted #aaa; padding:3px; overflow:hidden; float:left; }
    .cron .title { text-align:center; padding:5px; margin-bottom:20px; border-bottom:1px dotted #aaa; font-size:18px; }
    .cron .range { text-align:center; font-size:12px; }
    .cron [title] { cursor:help; }
    .taskname { width:120px; float:left; clear:left; padding:2px; background-color:#ddd; margin-bottom:1px; font-size:14px; }
    .noback { background-color:transparent; }
    .sched { float:left; background-color:#f8f8f8; }
    .sched span { float:left; border-left:1px solid transparent; height:21px; }
    .sched span.head { background-color:transparent; overflow:visible; font-size:10px; color:#999; white-space:nowrap; }
    .sched span.head.headtext:before { content:'\02193'; color:#000; } /* \02193 == down-arrow == &darr; */
    .sched span.run { background-color:#fb8; }
    .sched span.inprog { background-color:#fed; }
    .sched span.mon { border-left-color:#00f; }
    .sched span.day { border-left-color:#0a0; }
    .sched span.hour { border-left-color:#eee; }
    .sched span.now { border-left-color:#f00; border-left-style:dotted; }

    .lhs { float:left; }
    .rhs { overflow:auto; float:left; padding-bottom:10px; }
    .rhs .sched span { margin-bottom:1px; }
    
    .sched.example { background-color:#fff; padding:5px; border:1px dotted #aaa; margin:20px 0; }
    .sched.example p { color:#444; margin:0 0 5px 0; }
    .sched.example span { padding:3px 5px; margin:2px 10px; font-size:11px; height:auto; }

    h4 { margin:5px 0; font-size:15px; color:#444; }
    hr { clear:both; border:0; margin:10px 0; }
    </style>
</head>
<body>

<?php

//Set the following variables to true for the examples you want to show:
//---------------------------------------------------------------------
$ShowChart             = true;
$ShowStyles            = true;
$ShowTaskList          = true;
$ShowLastDateAllTasks  = true;
$ShowNextDatesFor1Task = true;
$ShowErrors            = true;
//---------------------------------------------------------------------
// For more information, see twzCron-doc.txt


require 'twzCronChart.php';

// Sample crontab lines. Usually you would read this from a file, 
// or use an empty string for twzCronChart to use your default crontab.
$MyCron='@daily php /home/user/reminders/reminder.php
33 15 * * * php /home/user/bin/a-task.php
10,40 10-22 * * * php /home/user/public_html/twzFW.php
15 5 * * * php /home/user/bin/backup.php';

$cron=new twzCronChart($MyCron);
$cron->SetChartName('cron schedule');
$cron->SetTaskNames(array('twzFW'=>'FileWatch', 'backup'=>'Backups', 'reminder'=>'Reminders'));
$cron->SetTaskDurations(array(2=>'5:00'));

if($ShowChart)
    {
    echo $cron->Chart('today 0:00', '+3 days', 900, 60, array('H:i D', 12*60));
    echo '<br style="clear:both;" />';
    }

if($ShowStyles)
    {
    $SpanClass=array('mon'=>'new month', 'day'=>'new day', 'hour'=>'new hour', 'now'=>'current time', 'run'=>'task start', 'inprog'=>'task in progress');
    echo '<div class="sched example"><p>Chart styles:</p>';
    foreach($SpanClass as $Class=>$Info)
        { echo '<span class="'.$Class.'">'.$Info.'</span>'; }
    echo "</div>\r\n";
    }

if($ShowTaskList)
    {
    echo '<hr /><h4>Task names</h4>';
    $Tasks=$cron->GetTaskNames();
    foreach($Tasks as $TaskId=>$TaskName)
        { echo $TaskId.' = '.$TaskName.'<br />'; }
    }

if($ShowLastDateAllTasks)
    {
    echo '<hr /><h4>Last run for each task</h4>';
    $Tasks=$cron->GetTaskNames();
    foreach($Tasks as $TaskId=>$TaskName)
        { echo $TaskName.': '.$cron->GetLastRun($TaskId, 'now').'<br />'; }
    }

if($ShowNextDatesFor1Task)
    {
    $TaskId=4; $Count=10; $NextDate='now';

    $NextDate=date('j M Y H:i', strtotime($NextDate));
    echo "<hr /><h4>Next $Count dates for task $TaskId (from $NextDate)</h4>";
    for($i=0; $i<$Count; $i++)
        {
        $NextDate=$cron->GetNextRun($TaskId, $NextDate);
        echo ' &nbsp; '.$NextDate.'<br />';
        $NextDate=strtotime($NextDate.' +1 minute');
        }
    }

if($ShowErrors)
    {
    $Errors=$cron->GetErrors();
    if(count($Errors)>0)
        {
        echo '<hr /><h4>Errors</h4>';
        foreach($Errors as $ThisError)
            { echo $ThisError.'<br />'; }
        }
    }


?>

</body>
</html>