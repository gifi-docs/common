/**
 * Created by GiFi on 21/05/2021.
 * author : florent.renier@gifi.Fr
 *
 * lib javascript needed :
 *
 *  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 *  <script src="https://unpkg.com/@reachfive/identity-core@1.19.0/umd/identity-core.min.js"></script>
 *  <script src="https://unpkg.com/@reachfive/identity-ui@1.11.7/umd/identity-ui.min.js"></script>
 *
 * Pour plus de sécurité
 * On englobe l'application dans une fonction anonyme qui s'execute immédiatement
 * Elle prend en paramètre l'objet jQuery pour pouvoir utiliser le symbole '$' sans risque de conflit
 *
 * @param  {object} $ Objet jQuery permettant ainsi d'éviter les conflits avec l'utilisation du '$'
 * @return {void}
 */

if(typeof gifi === 'undefined') {
    window.gifi={}
}

(function($) {


    /**
     * Objet contenant les fonctions / variables pour reach5 auth
     * @type {object}
     */
    gifi.auth = gifi.auth || {};

    /**
     * Simple chaîne de caractère contenant le numéro de version
     * @type {string}
     */
    gifi.auth.version = '1.0';


    /**
     * check if widget is displayed into screen
     * @type {bool}
     */
    gifi.auth.display = false;


    /**
     * Options par défaut
     * @type {object}
     */
    gifi.auth.defaults ={
        credentials:{
            domain:'gifi-test.reach5.net',
            clientId:'kbdZ4mXqSrjlEn5XZ75v'
        },
        connexionUri:'',
        loginUri:'',
        logOutUri:'',
        resetPasswordUri:'',
        identityCoreVersion:'1.19.0',
        identityUiVersion:'1.11.7',
        popin:{
            close:{
                overlay:true,
                button:true  // if (button = false and overlay =false) then button <- true
            }
        },
        widgetLoginConfig:{

            auth: {},
            allowWebAuthnSignup: true,
            allowForgotPassword: true,
            allowSignup:true,
            initialScreen: 'login',
            redirectUrl: '',
            signupFields: [
                {key: 'given_name', label: 'Prénom', required: true},
                {key: 'family_name', label: 'Nom', required: true},
                'email',
                'password',
                'password_confirmation',
                'consents.cgu'
               // 'custom_fields.loyalty_card_number'
            ],
            socialProviders: ['facebook', 'google', 'paypal'],
            showLabels: false,
            showRememberMe: true,
            countryCode: 'FR',
            theme: {
                primaryColor: '#274890',
                borderRadius: '25',
                socialButton: {
                    inline: true
                }
            },
            i18n: {
                email: 'Email'
            }
        },
        widgetResetPasswordConfig:{
            container: '',
            loginLink: '',
            canShowPassword: true,
            onReady: function(instance) {
                // Destroy the widget
                // if (...) instance.destroy()
            },
            theme: {
                primaryColor: '#274890',
                borderRadius: '#374890'
            },
            i18n: {
                // 'passwordReset.intro': 'Saisir le nouveau mot de passe',
                // 'newPassword': 'Nouveau mot de passe',
                // 'passwordConfirmation': 'Confirmer le nouveau mot de passe'
            }
        },
        headerButton : {
            showMenu:'mouseover', // event menu display mode : mouseover | click | none
            showMenuDelayOut:'2000', // if showMenu = 'mouseover', showMenuDelayOut is the delay before menu hide after mouseout
            container:'', // must be defined
            color:'#fff', // color apply to integrated svg icon
            connected:{
                imageUri:'', // default : use integrated svg icon
                underLabel:'' // ex : Bonjour #name#, where #name# id the user name
            },
            notConnected:{
                imageUri:'', // default : use integrated svg icon
                underLabel:'' //Me connecter
            }
        }

}
    /**
     * Tableau contenant les différentes configurations
     * @type {array}
     */
    gifi.auth.differedFunction = [];

    /**
     * Tableau contenant les différentes configurations
     * @type {array}
     */
    gifi.auth.settings = [];

    /**
     * var r5ClientApiCore : reach5 client api core
     * @type {object}
     */
    gifi.auth.sessionUser = null;

    /**
     * var r5ClientApiCore : reach5 client api core
     * @type {object}
     */
    gifi.auth.r5ClientApiCore = null;

    /**
     * var r5ClientApiUi : reach5 client api UI
     * @type {object}
     */
    gifi.auth.r5ClientApiUi = null;

    /**
     * Permet de définir des options par défaut pour l'ensemble des formulaires
     * Ce qui permet d'éviter de définir plusieurs fois des options qui sont identiques à chaque formulaire
     *
     * @param {object} options Options que l'on souhaite redéfinir
     * @return {void}
     */
    gifi.auth.setDefaultOptions = function( options )
    {
        var _this = this;

        options = options || {};

        $.extend(true, _this.defaults, options);
    };


    /**
     * check if variable setting exist, in global settings scope
     *
     * @param  {string} variable : variable setting name
     * @param  {bool} verbose : write log into console
     * @return {bool}
     */
    gifi.auth.settingExist = function(variable,verbose) {
        var _this = this;
        return _this.varExist('this.settings.'+variable,verbose);
    };

    /**
     * check if javascript variable exist
     *
     * @param  {string} variable : variable name
     * @param  {bool} verbose : write log into console
     * @return {bool}
     */
    gifi.auth.varExist = function(variable,verbose) {
        if((typeof verbose === 'undefined')) {
            verbose=false;
        }
        try {
            v1 = eval(variable);
            res = (typeof v1 !== 'undefined');
        } catch (err) {
            res = false;
        } finally {
            if (!res && verbose) {
                console.log('missing variable : ' + variable);
            }
            return res;
        }
    };

    /**
     * get variable setting value
     *
     * @param  {string} setting : variable setting name
     * @param  {string} defaultValue : default value
     * @return {object}
     */
    gifi.auth.getSettingValue = function(setting,defaultValue) {
        var _this = this;
        value = null;
        if((typeof defaultValue === 'undefined')) {
            defaultValue=null;
        }
        if (!_this.settingExist(setting) ) {
            value = defaultValue;
        } else {
            value = eval('_this.settings.'+setting);
        }
        return value;
    }

    /**
     * Initialisation de l'application
     *
     * @param  {object} settings Paramètres de l'utilisateur
     * @return {void}
     */
    gifi.auth.init = function(settings)
    {
        var _this = this;

        _this.settings = $.extend(true, _this.defaults, settings);


        // check reach credential api
        if (_this.settings.credentials.domain == '' || _this.settings.credentials.clientId == '') {
            console.error('Missing or bad reach5 api credential, check settings.credentials variables')
        } else {

            // add default reach5 login widget config
            if (!_this.settingExist('widgetLoginConfig')) {
                _this.settings.widgetLoginConfig = r5defaultConfigLoginWidget;
            }
            if (!_this.settingExist('widgetLoginConfig.auth')) {
                _this.settings.widgetLoginConfig.auth={};
            }

            // set redirectUri into reach5 widget config, if not exist --> window.location.href is used
            _this.settings.widgetLoginConfig.auth.redirectUri = _this.getSettingValue('loginUri',location.protocol + '//' + location.host + location.pathname);
            _this.settings.widgetLoginConfig.redirectUrl = _this.getSettingValue('resetPasswordUri',location.protocol + '//' + location.host + location.pathname);


            // check if there at least, one close popin method, even if we active  close popin button
            if( _this.getSettingValue('popin.close.button',true) == false && _this.getSettingValue('popin.close.overlay',true) == false) {
                _this.settings.popin.close.button=true;
            }
        }
    };


    /**
     * create instance r5 Client Api Ui
     *
     * @return {object}
     */
    gifi.auth.initR5ClientApiUi = function() {
        var _this = this;
        if (_this.r5ClientApiUi==null) {
            _this.r5ClientApiUi = reach5Widgets.createClient({
                domain: _this.settings.credentials.domain,
                clientId: _this.settings.credentials.clientId
            });
        }
        return _this.r5ClientApiUi;
    };

    /**
     * create instance r5 Client Api Core
     *
     * @return {object}
     */
    gifi.auth.initR5ClientApiCore = function(functionCallback) {
        var _this = this;
        if (_this.r5ClientApiCore==null) {
            _this.r5ClientApiCore = reach5.createClient({
                domain: _this.settings.credentials.domain,
                clientId: _this.settings.credentials.clientId
            });
            // _this.checkSession(functionCallback);
        }
        return _this.r5ClientApiCore;
    };

    /**
     * log out the user
     *
     * @param  {string} redirectTo : url to redirect after logout
     * @return {void}
     */
    gifi.auth.logout = function(redirectTo) {
        var _this = this;
        if (typeof redirectTo === 'undefined')  {
            redirectTo = _this.getSettingValue('logOutUri');
        }
        if (redirectTo != null) {
            _this.r5ClientApiCore.logout({
                redirectTo: redirectTo
            });
        }
    }

    /**
     * add style into dom
     *
     * @param  {strin} content : new style(s) to add
     * @return {void}
     */
    gifi.auth.addStyleToDom = function(content) {
        var parentElem = $('head style');
        if (!parentElem.length) {
            $('head').append('<style></style>');
            parentElem = $('head style');
        }
        parentElem.append(content);
    }


    /**
     * add elem into dom
     *
     * @param  {integer} id : element identifier to add into dom (add only if not already exist into DOM)
     * @param  {integer} parentId : parent element identifier container (default : body) (optionnal)
     * @param  {integer} _class : class name of the element (optionnal)
     * @param  {integer} innerHtml : content to insert into new element (optionnal)
     * @return {void}
     */
    gifi.auth.addToDom = function(id,parentElem,_class,innerHtml) {

        if (typeof id !== 'undefined') {
            if (typeof innerHtml === 'undefined') {
                innerHtml='';
            }
            if (typeof _class === 'undefined') {
                _class='';
            }

            content='<div id="'+id+'"'
            if (_class!='') {
                content+=' class="'+_class+'"';
            }
            content+='>'
            if (innerHtml != '') {
                content+=innerHtml;
            }
            content+='</div>';

            if (parentElem == undefined ) {
                parentElem = $('no-parent-id');
            }
            if (!parentElem.length) {
                parentElem = $("body");
            }
            if (!$('#' + id).length) {
                parentElem.append(content);
            }
        }
    }


    /**
     * show the reach5 widget on screen
     *
     * @param  {object} settings : settings value
     * @return {void}
     */
    gifi.auth.showLoginPopup = function(container) {

        var _this = this;

        if (!_this.isConnected() && _this.display==false) {
            if (typeof container === 'undefined') {
                container='r5-auth-popin-container';
            }
            containerWidget = container+'-widget';

            // init reach5 api ui
            _this.initR5ClientApiUi();

            // createand add reach5 widget container if not exist, into DOM
            _this.addToDom(container,$('body'),'r5-container-popup');
            _this.addToDom(containerWidget,$('#'+container),'r5-popup');


            if( _this.getSettingValue('popin.close.overlay',true)) {
                // manage event click on overlay
                $('#' + container).on('click', function (e) {
                    if ($(this).hasClass('not-close-popin')) {
                        $(this).removeClass('not-close-popin');
                    } else {
                        $(this).remove();
                    }
                });

                // manage event click in reach5 popin
                $('#' + containerWidget).on('click', function (e) {
                    $('#' + container).addClass('not-close-popin');
                });
            }

            config = _this.settings.widgetLoginConfig;

            config.container = containerWidget;

            // display close popin button
            if( _this.getSettingValue('popin.close.button',true)) {
                config.onReady = function() {
                    // add close popin button
                    _this.addToDom('r5-btn-close-popin',$('#'+containerWidget),'',_this.getImageSvg('account-button-cross'));
                    // manage event click close popin button
                    $('#' + containerWidget+ ' #r5-btn-close-popin').on('click',function(e){
                        $('#' + container).remove();
                    });
                }
            }

            // call reach5 widget api
            _this.r5ClientApiUi.showAuth(config);
        }
    }



    /**
     * show reset password widget reach5
     *
     * @param  {string} container : DOM element id
     * @param optionnal {bool} redirect : if true and user is already connected , automatically redirected to after connexion page
     * @return {void}
     */
    gifi.auth.showPasswordReset = function(container,redirect,loginLink) {

        var _this = this;
        if (typeof redirect !== 'undefined') {
            redirect = false;
        }
        if (redirect !== true || redirect !== false) {
            redirect = false;
        }

        if (!_this.isConnected()) {

            // init reach5 api ui
            _this.initR5ClientApiUi();

            config = _this.settings.widgetResetPasswordConfig;

            if (typeof container !== 'undefined') {
                config.container = container;
            }
            if (config.container == ''){
                config.container = 'r5-auth-reset-password-container';
            }
            if (typeof loginLink !== 'undefined') {
                config.loginLink = loginLink;
            } else {
                config.loginLink = _this.settings.connexionUri;
            }
            if (config.loginLink == ''){
                console.log('Missing reset password login link')
            } else {
                config.redirectUrl = _this.settings.resetPasswordUri;
                _this.r5ClientApiUi.showPasswordReset(config);
            }
        } else {
            if (redirect) {
                window.location.assign(_this.settings.loginUri);
            }
        }

    }

    /**
     * show the reach5 widget on screen
     *
     * @param  {object} settings : settings value
     * @return {void}
     */
    gifi.auth.showAccountButton = function(settings) {
        var _this = this;
        var html='';
        var id = 'r5-account-button';

        _this.settings = $.extend(true, _this.settings, settings);

        if (!_this.isConnected()){
            if(_this.settings.headerButton.notConnected.imageUri != ''){
                html+='<img src="'+_this.settings.headerButton.notConnected.imageUri+'" alt="Se connecter">'
            } else {
                html+=_this.getImageSvg('account-button-not-connected');
            }
            if(_this.settings.headerButton.notConnected.underLabel) {
                html+='<div class="under-label">'+_this.settings.headerButton.notConnected.underLabel+'</div>';
            }
        } else {
            if(_this.settings.headerButton.connected.imageUri != ''){
                html+='<img src="'+_this.settings.headerButton.connected.imageUri+'" alt="Mon compte">'
            } else {
                html+=_this.getImageSvg('account-button-connected');
            }
            if(_this.settings.headerButton.connected.underLabel) {
                str = _this.settings.headerButton.connected.underLabel;
                str = str.replace('#name#',_this.getUserSessionName());
                html+='<div class="under-label">'+str+'</div>';
            }

            // ajout du menu au survol ou click
            menuHtml='<div class="panel header-small-panel" style="display: none;">\n' +
                '\t<div class="head">\n' +
                '\t\t<a class="btn-small-close" href="javascript:$(\'.header-small-panel\').hide()">'+_this.getImageSvg('account-button-cross')+'</a>\n' +
                '\t</div>\n' +
                '\t <div id="plugin_account_block" class="connecter">\n';

            // ajout des items du menu
            items = _this.getSettingValue('menu.items','[]');
            nbItem = 0 ;
            i = 0;

            // count enabled items
            items.forEach(function (item) {
                disabled = false;
                if (typeof item.disabled !== 'undefined') {
                    disabled = item.disabled;
                }
                if (!disabled) {
                    nbItem++;
                }
            });

            // add enabled items
            items.forEach(function (item){
                disabled = false;
                if (typeof item.disabled !== 'undefined') {
                    disabled = item.disabled;
                }
                last = (i<(nbItem-1))?'':'last';
                if (!disabled) {
                    menuHtml+='              <div class="lien-container '+last+'">\n' +
                        '                        <div class="lien '+last+'">\n' +
                        '                            <img class="picto" src="'+item.icon+'">\n' +
                        '                            <a href="'+item.uri+'">\n' +
                        '                                <span>'+item.label+'</span>\n' +
                        '                            </a>\n' +
                        '                        </div>\n' +
                        '                    </div>'
                }

                if (i<(nbItem-1)) {
                    menuHtml+='              <div class="sep"></div>';
                }
                i++;
            });

            menuHtml+='\t</div>\n' +
                '</div>\n'

            html+=menuHtml;
        }
        html+='</div>';


        var container = _this.settings.headerButton.container;
        var containerElem = $(container);
        if (!containerElem.length){
            container = '#'+container;
            containerElem = $(container);
            if (!containerElem.length){
                container = '.'+container;
                containerElem = $(container);
            }
        }
        if (containerElem.length) {


            // add style css

            color = _this.settings.headerButton.color;
            if (color) {
                css= container+' '+".under-label {color:"+color+"}\n"+
                    container+' '+"svg {fill:"+color+"}\n";
                    container+' '+"img {fill:"+color+"}\n";
                _this.addStyleToDom(css);
            }


            // add button into container
            _this.addToDom(id,containerElem, '', html);

            // add action on click
            if (!_this.isConnected()) {
                $('#' + id).on('click', function () {
                    gifi.auth.showLoginPopup();
                })
            } else {
                showMenu = _this.getSettingValue('headerButton.showMenu','none');     // mouseover, click, none
                switch(showMenu) {
                    case 'mouseover' :
                        delay = _this.getSettingValue('headerButton.showMenuDelayOut','5000');
                        $('#' + id).r5hoverDelay({

                            delayIn: 200,
                            delayOut: delay,
                            handlerIn: function($element){
                                $element.find('.panel').show();
                            },
                            handlerOut: function($element){
                                $element.find('.panel').hide();
                            }
                        });
                        break;
                    case 'click' :
                        $('#' + id).on(showMenu, function () {
                            $('#'+id+' .panel').show();
                        });
                        break;
                    default :
                        $('#' + id).on('click', function () {
                            window.location.assign(_this.getLoginUri())
                        });

                }

            }
        }
    }


    /**
     * show the reach5 widget on screen into a DOM element
     *
     * @param  {string} container : DOM element id
     * @param  {bool} redirect : if user is already connected , automatically redirected to after connexion page
     * @return {void}
     */
    gifi.auth.showLogin = function(container,redirect) {

        var _this = this;
        if (typeof redirect !== 'undefined') {
            redirect = false;
        }
        if (redirect !== true || redirect !== false) {
            redirect = false;
        }

        if (!_this.isConnected()) {


            if (typeof container !== 'undefined') {
                _this.settings.widgetLoginConfig.container = container;
            }
            if (_this.settings.widgetLoginConfig.container == ''){
                _this.settings.widgetLoginConfig.container = 'r5-auth-login-container';
            }

            // init reach5 api ui
            _this.initR5ClientApiUi();

            // createand add reach5 widget container if not exist, into DOM
            _this.addToDom(_this.settings.widgetLoginConfig.container);

            // display reach5 layout widget
            $('#' + _this.settings.container).addClass('r5-container-no-popup');

            // display reach5 layout widget
            _this.r5ClientApiUi.showAuth(_this.settings.widgetLoginConfig);
            _this.display = true;
        } else {
            if (redirect) {
                window.location.assign(_this.settings.loginUri);
            }
        }
    }

    /**
     * check if there is an active session user, and execute action switch the result
     *
     * @param  {function} fucntionCallback : executed function after check the session user
     * @return {void}
     */
    gifi.auth.checkSession = function() {
        var _this = this;
        var result = null;
        res = _this.r5ClientApiCore.checkSession();
        _this.r5ClientApiCore.checkSession() // The nonce links the retrieved id token with the local session
            .then( function(authResult) {
                _this.sessionUser = {
                    name:authResult.idTokenPayload.givenName,
                    authResult:authResult
                };
            })
            .catch(function() {
                _this.sessionUser =null;
            })
            .finally(function(){
                _this.execDifferedFunctions();
            });
    }

    /**
     * get if there is user is connected
     *
     * @return {bool}
     */
    gifi.auth.isConnected = function() {
        var _this = this;
        return (_this.sessionUser!=null);
    }


    /**
     * get if there is user is connected
     *
     * @return {bool}
     */
    gifi.auth.getLoginUri = function() {
        var _this = this;
        return _this.getSettingValue('loginUri',location.protocol + '//' + location.host + location.pathname);
    }

    /**
     * get if user session name if he is connected
     *
     * @return {string}
     */
    gifi.auth.getUserSessionName = function() {
        var _this = this;
        if (_this.isConnected()){
            return _this.sessionUser.name;
        }
        return '';
    }

    /**
     * get if user session name if he is connected
     *
     * @return {string}
     */
    gifi.auth.afterCheckSession = function(functionCallback) {
        var _this = this;
        _this.differedFunction.push(functionCallback);
        return '';
    }

    /**
     * execute diffred function into the stack
     *
     * @return {string}
     */
    gifi.auth.execDifferedFunctions = function() {
        var _this = this;
        _this.differedFunction.forEach(function (f){
            f(_this.sessionUser);
        });
    }


    /**
     * execute diffred function into the stack
     *
     * @return {string}
     */
    gifi.auth.getImageSvg = function(imageName) {
        svg = '';
        switch(imageName) {
            case 'account-button-connected':
                svg='<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">\n' +
                    '  <g id="svg_1">\n' +
                    '   <path id="svg_2" d="m929.2,990c-16.9,0 -30.6,-13.7 -30.6,-30.6c0,-211 -178.8,-382.7 -398.6,-382.7c-219.8,0 -398.6,171.7 -398.6,382.7c0,16.9 -13.7,30.6 -30.6,30.6c-16.9,0 -30.6,-13.7 -30.6,-30.6c0,-244.8 206.3,-444 459.8,-444c253.6,0 459.9,199.2 459.9,444c0,16.9 -13.7,30.6 -30.7,30.6z"/>\n' +
                    '   <path id="svg_3" d="m503.8,576.7c-156.2,0 -283.3,-127.1 -283.3,-283.3s127.1,-283.4 283.3,-283.4c156.2,0 283.3,127.1 283.3,283.3s-127.1,283.4 -283.3,283.4zm0,-505.4c-122.4,0 -222.1,99.6 -222.1,222.1s99.6,222.1 222.1,222.1s222.1,-99.6 222.1,-222.1s-99.7,-222.1 -222.1,-222.1z"/>\n' +
                    '   <g stroke="null" id="svg_6">\n' +
                    '    <path stroke="null" d="m642.84357,927.48956c-9.74166,-1.43304 -16.58498,-7.75528 -23.18677,-14.75188c-44.44132,-46.70024 -88.96314,-93.23188 -133.48497,-139.76352c-7.08485,-7.41809 -10.62727,-15.93203 -9.74167,-26.46909c1.04663,-12.64447 11.91543,-24.53027 23.91136,-26.21619c10.7883,-1.51734 19.40283,2.3603 26.89022,10.19987c39.12768,41.13666 78.41638,82.10474 117.54407,123.24141c0.96611,1.01156 1.52968,2.276 2.89834,4.38342c1.6102,-2.19171 2.33478,-3.54046 3.3009,-4.55201c84.53511,-88.51128 168.98972,-176.93826 253.44432,-265.44953c5.47466,-5.73216 11.59339,-9.6098 19.48333,-10.53706c19.48333,-2.3603 35.82679,17.61796 30.67417,37.42763c-1.61019,6.32223 -5.0721,11.38002 -9.41962,15.84773c-73.34428,76.70977 -146.76906,153.50384 -220.11334,230.29791c-17.06804,17.87085 -34.21659,35.6574 -51.20413,53.69684c-5.95771,6.32223 -12.5595,11.04283 -21.01301,12.64447c-3.3009,0 -6.6823,0 -9.9832,0l0,0z" id="svg_5"/>\n' +
                    '  </g>\n' +
                    '</svg>';
                break;
            case 'account-button-not-connected':
                svg='<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">\n' +
                    '<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>\n' +
                    '<g><path d="M929.2,990c-16.9,0-30.6-13.7-30.6-30.6c0-211-178.8-382.7-398.6-382.7c-219.8,0-398.6,171.7-398.6,382.7c0,16.9-13.7,30.6-30.6,30.6c-16.9,0-30.6-13.7-30.6-30.6c0-244.8,206.3-444,459.8-444c253.6,0,459.9,199.2,459.9,444C959.9,976.3,946.2,990,929.2,990z"/><path d="M503.8,576.7c-156.2,0-283.3-127.1-283.3-283.3S347.6,10,503.8,10C660,10,787.1,137.1,787.1,293.3S660,576.7,503.8,576.7z M503.8,71.3c-122.4,0-222.1,99.6-222.1,222.1s99.6,222.1,222.1,222.1s222.1-99.6,222.1-222.1S626.2,71.3,503.8,71.3z"/></g>\n' +
                    '</svg>'
                break;
            case 'account-button-cross':
                svg='<svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">\n' +
                    '<defs><style>.cls-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;}</style></defs>\n' +
                    '<g id="cross"><line class="cls-1" x1="7" x2="25" y1="7" y2="25"/><line class="cls-1" x1="7" x2="25" y1="25" y2="7"/></g>\n' +
                    '</svg>'
                break;
        }
        return svg;
    }




    $.fn.r5hoverDelay = function(options) {
        var defaultOptions = {
            delayIn: 300,
            delayOut: 300,
            handlerIn: function(){},
            handlerOut: function(){}
        };
        options = $.extend(defaultOptions, options);
        return this.each(function() {
            var timeoutIn, timeoutOut;
            var $element = $(this);
            $element.hover(
                function() {
                    if (timeoutOut){
                        clearTimeout(timeoutOut);
                    }
                    timeoutIn = setTimeout(function(){options.handlerIn($element);}, options.delayIn);
                },
                function() {
                    if (timeoutIn){
                        clearTimeout(timeoutIn);
                    }
                    timeoutOut = setTimeout(function(){options.handlerOut($element);}, options.delayOut);
                }
            );
        });
    };





    // Ajout de l'objet dans le DOM
    window.gifi.auth = gifi.auth;

    // Lorsque la page est chargée, on check la session
    $(document).ready(function() {
        jsCoreUri = "https://unpkg.com/@reachfive/identity-core@"+gifi.auth.getSettingValue('identityCoreVersion','1.19.0')+"/umd/identity-core.min.js";
        jsUiUri = "https://unpkg.com/@reachfive/identity-ui@"+gifi.auth.getSettingValue('identityUiVersion','1.11.7')+"/umd/identity-ui.min.js";
        $.getScript(jsCoreUri, function() {
            $.getScript(jsUiUri, function() {
                //alert("Script loaded and executed.");
                gifi.auth.initR5ClientApiCore();
                gifi.auth.checkSession();
            });
        });

    });

})(jQuery);