<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/r5/authentification.js"></script>
<link rel="stylesheet" type="text/css" href="css/authentification.css" media="all">
<style>
    html, body {
        margin:0;
        padding:0;
    }
    a {
        text-decoration:none;
    }

    a:visited,a:active,a:hover,a:link {
        color: inherit;
    }
    .navbar-menu {
        background-color: #1b2d34;
        padding:10px 20px;
        color:#fff;
        display: flex;
        align-items: center;
        height:80px;
    }

    #title-page {
        font-size: 28px;
        color: #A5FF00;
        font-weight: bold;
        flex: 1;
    }
    .navbar-menu .navbar-link {
        text-align: center;
        display:inline-block;
        width: 60px;
        cursor:pointer;
    }

</style>


<div id="topbar-menu" class="navbar-menu">
    <div id="title-page" ><a href="home.php" style="">Réinitialisation mot de passe</a></div>
    <div class="navbar-item navbar-links">
        <div id="auth2" class="navbar-link">
        </div>
    </div>
</div>
<div id="gifi-auth-reset-password-container"></div>
<script>
    gifi.auth.init({
        connexionUri:'http://magento/r5/connexion.php',
        logOutUri:'http://magento/r5/home.php',
        loginUri: 'http://magento/r5/account.php',
        resetPasswordUri: 'http://magento/reset-password.php',
        redirectUrlConfirmEmail: 'http://magento/r5/account.php',
        headerButton : {
            showMenu:'mouseover', // mouseover, click, none
            showMenuDelayOut:'30000', // if showMenu = 'mouseover', showMenuDelayOut = delay before menu hide after mouseout
            container:'auth2',
        },
        menu:{
            items:[
                {
                    label:'Mes commandes',
                    uri:'https://livraison.gifi.fr/sales/order/history/',
                    disabled:false,
                    icon:'https://livraison.gifi.fr/skin/frontend/base/default/images/svg/orders.svg'
                },
                {
                    label:'Ma carte VIP',
                    uri:'https://livraison.gifi.fr/vip/customer/view/',
                    disabled:false,
                    icon:'https://livraison.gifi.fr/skin/frontend/base/default/images/svg/viewvip.svg'
                },
                {
                    label:'Mes infos personnelles',
                    uri:'https://livraison.gifi.fr/customer/account/edit/',
                    disabled:false,
                    icon:'https://livraison.gifi.fr/skin/frontend/base/default/images/svg/account_edit.svg'
                },
                {
                    label:'Déconnexion',
                    uri:'javascript:gifi.auth.logout()',
                    disabled:false,
                    icon:'https://livraison.gifi.fr/skin/frontend/base/default/images/svg/deconnexion.svg'
                }
            ]
        }
    });

    gifi.auth.afterCheckSession(function (){
        gifi.auth.showAccountButton();
        gifi.auth.showPasswordReset('gifi-auth-reset-password-container');
    });

</script>
