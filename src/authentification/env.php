<?php

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);
define('BP', __DIR__);
define('DIR_LOG', BP . DS . 'log');

date_default_timezone_set('Europe/Paris');


// Set include path
$paths = array();
$paths[] = BP . DS . 'app' . DS . 'code' ;
$appPath = implode(PS, $paths);
set_include_path($appPath);

require_once('autoload.php');
Varien_Autoload::register();
?>

