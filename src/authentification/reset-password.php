<?php
include 'head.php';
include 'menu.html'
?>

<div id="gifi-auth-reset-password-container">
</div>

<script>
    setTitle('Réinitialisation mot de passe');

    gifi.auth.afterCheckSession(function () {
       gifi.auth.showPasswordReset('gifi-auth-reset-password-container');
    });

</script>